const {pathOr} = require('ramda');
const app = require('./src/app');

const PORT = pathOr(3000, ['env', 'PORT'], process);

app.listen(PORT, () => console.info(`App started on ${PORT} port`));
