module.exports = {
  verbose: true,
  collectCoverage: true,
  testMatch: ['<rootDir>/src/**/__test__/*.spec.js'],
};
