module.exports = {
  parserOptions: {
    ecmaVersion: 6,
    sourceType: 'module',
  },
  rules: {
    semi: 2,
    quotes: ['error', 'single'],
  },
};
