const notFound = function notFound(req, res) {
  console.info(`404 - ${req.path}`);
  res.status(404)
    .json({message: 'Service not found!'});
};

module.exports = notFound;
