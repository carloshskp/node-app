const health = function(router) {
  router.get('/', (req, res) => {
    console.info('Application health called');
    res.json({status: 'health'});
  });
};

module.exports = health;
