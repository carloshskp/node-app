const enrouten = require('express-enrouten');
const express = require('express');

const notFoundMiddleware = require('./middlewares/not-found');

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(enrouten({ directory: 'controllers/' }));
app.use(notFoundMiddleware);

module.exports = app;
